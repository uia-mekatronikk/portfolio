# Install Git and Hugo
1) Download and install [Git](https://git-scm.com/)
1) Download the latest release of Hugo extended from https://github.com/gohugoio/hugo/releases
1) Unzip and move the downloaded Hugo binary file to a location visible to your system path

# Develop website locally using Hugo server
Open a terminal in the root directory and execute `hugo server -D`.

# Example webapge
https://uia-mekatronikk.gitlab.io/portfolio

# Check out my portfolio for more inspiration
https://sondrest.gitlab.io/portfolio/

# Change the theme
Check out other themes at https://themes.gohugo.io/
