+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = "ASSOCIATIONS THAT HELPED TO GATHER EXPERIENCE"

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Chief Evangelist Officer"
  company = "Pied Piper"
  company_url = "http://www.piedpiper.com/"
  location = "Palo Alto, California"
  date_start = "2016-01-01"
  date_end = ""
  description = """The master of all things media for Pied Piper.
  """

[[experience]]
  title = "Founder and CEO"
  company = "Bachmann Business Incubator"
  company_url = ""
  location = "Palo Alto, California"
  date_start = "2014-04-01"
  date_end = "2016-04-01"
  description = """A live-in startup business incubator run by entrepreneur Erlich Bachman (That's me.)
  """

[[experience]]
  title = "CEO"
  company = "Aviato"
  company_url = ""
  location = "Palo Alto, California"
  date_start = "2009-02-01"
  date_end = "2014-01-01"
  description = """Founder and CEO of Aviato, a fantastic Silicon Valley Startup
  """

+++
